#!/usr/bin/env python
# -*- coding: utf-8 -*-

from conan.packager import ConanMultiPackager

import os

def main():
    builder = ConanMultiPackager(reference=get_package_reference())

    # Header-only no options
    builder.add(options={})

    builder.run()

#####################################################################

def get_package_reference():
    pkg_info = get_package_info()

    return "{}/{}".format(pkg_info[0], pkg_info[1])

def get_package_info():
    import inspect
    import conanfile

    for name, member in inspect.getmembers(conanfile, is_recipe):
         if member.__module__ == conanfile.__name__:
             return (member.name, member.version)

    return None

def is_recipe(member):
    import inspect
    from conans import ConanFile

    return inspect.isclass(member) and issubclass(member, ConanFile)

if __name__ == "__main__":
    main()
