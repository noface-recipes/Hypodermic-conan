/** Example to test FakeIt package
 * Adapted from example on FakeIt README
*/

#include <iostream>
#include <Hypodermic/Hypodermic.h>

using namespace std;

struct Greeter {
    virtual void greet(const std::string & name) = 0;
};

struct HelloGreeter : public Greeter{
    void greet(const std::string & name) override{
        cout << "Hello, " << name << endl;
    }
};

std::shared_ptr< Hypodermic::Container > configureDependencies(){
    Hypodermic::ContainerBuilder builder;

    builder.registerType<HelloGreeter>().as<Greeter>();

    return builder.build();
}


int main() {
    std::cout<<"*** Running Hypodermic example ***" << std::endl;

    auto container = configureDependencies();

    container->resolve<Greeter>()->greet("world");

    std::cout<<"**********************************" << std::endl;

    return 0;
}
