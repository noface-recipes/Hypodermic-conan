from conans import ConanFile, tools
from os import path

class Hypodermic(ConanFile):
    name        = "Hypodermic"
    version     = "2.4"
    license     = "MIT"
    description = "Hypodermic is an IoC container for C++. It provides dependency injection to your existing design."
    url         = "https://gitlab.com/no-face/Hypodermic-conan"
    repo        = "https://github.com/ybainier/Hypodermic"

    build_policy = "missing" #header-only, so it is ok

    requires = (
        "Boost/1.64.0@conan/stable"
    )
    default_options = (
        "Boost:header_only=True"
    )

    EXTRACTED_FOLDER = name + "-" + version
    ZIP_URL_NAME = version + '.tar.gz'
    SHA = "f8202cc2fb967f43636f8f8c92f0e7e99b0b69e930603eb1a64a1083b66adba1"

    def source(self):
        # Clone the repository
        zip_name = self.ZIP_URL_NAME
        url = "{}/archive/v{}".format(self.repo, self.ZIP_URL_NAME)

        tools.download(url, zip_name)
        tools.check_sha256(zip_name, self.SHA)
        tools.untargz(zip_name)

    def package(self):
        self.copy("*", dst=path.join("include", "Hypodermic"), src=path.join(self.EXTRACTED_FOLDER, 'Hypodermic'))

    def package_info(self):
        pass

